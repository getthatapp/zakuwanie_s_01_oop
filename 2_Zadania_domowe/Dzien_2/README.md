<img alt="Logo" src="http://coderslab.pl/wp-content/themes/coderslab/svg/logo-coderslab.svg" width="400">

# Programowanie obiektowe w Ruby &ndash; praca domowa.

> Kilka ważnych informacji

Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki

## Jak zacząć?

1. Stwórz [*fork*][forking] repozytorium z zadaniami.
2. [*Sklonuj*][ref-clone] repozytorium na swój komputer.
3. Rozwiąż zadania i [*skomituj*][ref-commit] zmiany do swojego repozytorium.
4. [*Wypchnij*][ref-push] zmiany do swojego repozytorium na GitHubie.
5. Stwórz [*pull request*][pull-request] do oryginalnego repozytorium, gdy skończysz wszystkie zadania.

# Dzień 2

Uwaga! Przed wysłaniem zadań, sprawdź je przy użyciu Rubocopa.

### Zadanie 1

1. Napisz klasę `ShoppingCart`. Klasa ta ma posiadać następujące zmienne instancyjne:
  * `products` - hash z obiektami klasy `Product`.

Do tych zmiennych nie powinno być ustawionych getterów, ani setterów.

 Klasa powinna mieć też nastepujące metody:
  * `add_product(new_product)` - metoda ta powinna dodawać nowy produkt do hasha z produktami. Kluczem produktu powinno być jego id (dzięki temu będziemy mogli łatwo znaleźć produkt w naszym koszyku). Jeżeli dodawany obiekt nie jest produktem, rzuć wyjątek ArgumentError. Przy dodawaniu drugiego egzemplarza produktu pamiętaj o zwiększaniu jego ilości.
  * `remove_product(product_id)` - metoda ta powinna usuwać wszystkie sztuki produktu z koszyka. Jeśli taki produkt nie był wcześniej zeskanowany, to ma nic nie robić.
  * `change_product_quantity(product_id, new_quantity)` - metoda ta powinna zmianiać ilość danego produktu w koszyku. Jeśli taki produkt nie był wcześniej zeskanowany, to ma nic nie robić.
  * `print_receipt` - metoda drukująca paragon. Na paragonie powinno się znaleźć: lista wszystkich produktów, wraz z ich id, nazwą, ceną, ilością i łączną ceną (pamiętaj że masz do tego dedykowaną metodę w klasie `Product`) i łączna kwota za wszystkie produkty znajdujące się w koszyku. Zwracaj ciąg znaków w postaci:

```
1: Produkt 1, 2x2.5 = 5.0
2: Produkt 2, 3x12.0 = 28.8
3: Produkt 3, 1x1 = 1
---
Razem: 38.8
```
Komunikat zakończ znakiem nowej linii.

**Wskazówka: do znaku nowej linii używaj ciągu znaków `\n`.**

2. Zmodyfikuj klasę produktu tak, żeby umożliwiała nadawanie rabatu. Jeżeli ilość danego produktu jest większa lub równa 3 to metoda ```get_total_sum()``` powinna nadawać 20% zniżki na łączną kwotę za te produkty.

**Uwaga: Obie klasy zapisz w tym folderze, nazwij je `product.rb` i `shopping_cart.rb`.**

### Zadanie 2
Napisz kod klasy o nazwie `TodoList` symulującej listę zadań. Wewnętrznie zadania przechowuj w tablicy (niedostępnej z zewnątrz) nazwanej `tasks`.
Niech klasa posiada metody instancyjne:
* add(string) - dodaje zadanie do listy
* list_all - zwraca wszystkie zadania w postaci tablicy
* get(int) - pobiera zadanie o konkretnym indeksie (w przypadku braku elementu o tym indeksie - zwracany jest nil)
* remove(int) - usuwa zadanie o konkretnym indeksie (w przypadku braku elementu o tym indeksie - nic się nie dzieje)

**Uwaga: Zapisz klasę w pliku `todo_list.rb`.**

Z gwiazdką: udostępnij możliwość pobierania elementów swojej klasy przez nawiasy kwadratowe (czyli `task_list[0]`)

Podpowiedź: http://stackoverflow.com/questions/10018900/how-does-defining-square-bracket-method-in-ruby-work

Podpowiedź 2: używaj jako indeksów w swojej klasie, indeksów tablicy pod spodem.

<!-- Links -->
[forking]: https://guides.github.com/activities/forking/
[ref-clone]: http://gitref.org/creating/#clone
[ref-commit]: http://gitref.org/basic/#commit
[ref-push]: http://gitref.org/remotes/#push
[pull-request]: https://help.github.com/articles/creating-a-pull-request
[ref-multiple-forms]: http://stackoverflow.com/a/14071321
