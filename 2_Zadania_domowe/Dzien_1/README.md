<img alt="Logo" src="http://coderslab.pl/wp-content/themes/coderslab/svg/logo-coderslab.svg" width="400">

# Programowanie obiektowe w Ruby &ndash; praca domowa.

> Kilka ważnych informacji

Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki

## Jak zacząć?

1. Stwórz [*fork*][forking] repozytorium z zadaniami.
2. [*Sklonuj*][ref-clone] repozytorium na swój komputer.
3. Rozwiąż zadania i [*skomituj*][ref-commit] zmiany do swojego repozytorium.
4. [*Wypchnij*][ref-push] zmiany do swojego repozytorium na GitHubie.
5. Stwórz [*pull request*][pull-request] do oryginalnego repozytorium, gdy skończysz wszystkie zadania.


# Dzień 1

### Zadanie 1

Napiszemy podstawę obiektowego programu, który będzie wspomagać skanowanie produktów w sklepie.

Swoje rozwiązanie zapisuj w pliku `product.rb`.

Stwórz klasę `Product`. Klasa ta ma posiadać podane atrybuty:
  * `id` - liczba całkowita. Powinna być unikalna w całym systemie (jak to osiągnąć będzie wyłumaczone w dalszej części zadania).
  * `name` - string. Jest to nazwa danego produktu.
  * `description` - string. Jest to opis danego produktu.
  * `price` - float. Jest to cena za jeden produkt. Powinna być większa od `0.01`. Sprawdź ten warunek w własnoręcznie utworzonym setterze. Jeśli warunek nie będzie spełniony - zostaw starą wartość lub nil (jeżeli była pusta).
  * `quantity` - liczba całkowita większa od zera.

**Uwaga: Nie waliduj typu tworzonych atrybutów (z wyjątkiem price), przyjmij, że klasa będzie je dostawać poprawnie.**

Klasa powinna mieć też nastepujące metody:
  * konstruktor - powinien przyjmować opis, cenę i ilość produtku.
  * atrybut id powiniem mieć możliwość wyłącznie odczytu.
  * metodę ```get_total_sum()``` która będzie zwracała łączną kwotę za dany produkt (wyliczaną jako ilość * cena produktu.

 ####Generowanie unikalnego id dla produktu:
 W dalszej części programu będziemy chcieli identyfikować nasze produkty po ich id. Dlatego musimy zagwarantować że każdy z stworzonych produktów będzie miał unikalny numer identyfikacyjny. W tym celu powinniśmy zdefiniować zmienną klasową ```next_id```.

 Zmienna ta będzie trzymała id ktore zostanie nadane następnemu stworzonemu produktowi. Nastepnie w konstruktorze klasy musimy wykonać następujące czynności:
  * własnie tworzonemu produktowi przypisać id trzymane w zmiennej `next_id`,
  * zwiększyć wartość  `next_id` o jeden.

 ```ruby
 # w konstruktorze
@id = @@next_id
@@next_id += 1
```

Dzięki temu żaden z naszych produktów nie będzie miał takiego samego id.

<!-- Links -->
[forking]: https://guides.github.com/activities/forking/
[ref-clone]: http://gitref.org/creating/#clone
[ref-commit]: http://gitref.org/basic/#commit
[ref-push]: http://gitref.org/remotes/#push
[pull-request]: https://help.github.com/articles/creating-a-pull-request
[ref-multiple-forms]: http://stackoverflow.com/a/14071321
