# Podstawowa obiektowość &ndash; zadania.

### Zadanie 1 &ndash; zadanie rozwiązywane z wykładowcą.
Stwórz klasę ```Calculator```. Konstruktor ma nie przyjmować żadnych danych. Każdy nowo stworzony obiekt powinien mieć pustą listę, w której będzie trzymać historię wywołanych operacji (stwórz ją w konstruktorze).
Następnie dodaj do klasy następujące metody:

1. ```add(num1, num2)``` &ndash; metoda ma dodać do siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "added ```num1``` to ```num2``` got ```result```".
2. ```multiply(num1, num2)``` &ndash; metoda ma pomnożyć przez siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "multiplied ```num1``` with ```num2``` got ```result```".
3. ```subtract(num1, num2)``` &ndash; metoda ma odjąć od siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "subtracted ```num1``` from ```num2``` got ```result```".
4. ```divide(num1, num2)``` &ndash; metoda ma podzielić przez siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "divided ```num1``` by ```num2``` got ```result```". Pamiętaj, że nie można dzielić przez zero.
5. ```printOperations()``` &ndash; metoda ma wypisać wszystkie zapamiętane operacje.
6. ```clearOperations()``` &ndash; metoda ma wyczyścić wszystkie zapamiętane operacje.

Pamiętaj o używaniu ```self``` w odpowiednich miejscach.
Stwórz kilka kalkulatorów i przetestuj ich działanie.

---

### Zadanie 2.
Stwórz klasę `Shape`, która ma spełniać następujące wymogi:

1. Mieć prywatne zmienne instancyjne:
`x`, `y` (określające środek tego kształtu) i `color`.
2. Posiadać konstruktor przyjmujący zmienne określające wartości `x`, `y` i `color`. Konstruktor powinien wypisywać informacje o właśnie stworzonym kształcie. Pamiętaj o sprawdzeniu:
 * czy podane zmienne są liczbami (jeżeli nie są, to nastaw oba na **0**),
 * czy podany kolor jest napisem.

3. Mieć funkcję wypisującą informacje o tym kształcie.
4. Mieć funkcję zwracającą odległość od innego kształtu.

Sprawdź, co się dzieje, kiedy zmieniasz dostęp do poszczególnych funkcji z publicznych na prywatne, i na chronione.

### Zadanie 3.
Stwórz klasę `BankAccount`, która ma spełniać następujące wymogi:

1. Mieć prywatne atrybuty:
 * `number` - atrybut ten powinien trzymać numer identyfikacyjny konta (dla uproszczenia możemy założyć że numerem konta może być dowolna liczba całkowita),
 * `cash` - atrybut określający ilość pieniędzy na koncie. Ma to być liczba zmiennoprzecinkowa.
2. Posiadać konstruktor przyjmujący tylko numer konta. Atrybut `cash` powinien być zawsze nastawiany na 0.0 dla nowo tworzonego konta.
3. Posiadać getery do atrybutów `number` i `cash`, ale NIE posiadać do nich seterów (nie chcemy żeby raz stworzone konto mogło zmienić swój numer, a do atrybutu `cash` dodamy specjalne funkcje modyfikujące jej wartość).
4. Posiadać metodę `deposit_cash(amount)` której rolą będzie zwiększenie wartości atrybutu `cash` o podaną watość. Pamiętaj o sprawdzeniu czy podana wartość jest:
 * Wartością numeryczną,
 * Wieksza od 0.0
5. Posiadać metodę `withdraw_cash(amount)` której rolą będzie zmniejszenie wartości atrybutu `cash` o podaną watość. Metoda ta powinna zwracać ilość wypłaconych pieniędzy. Dla uproszczenia zakładamy że ilośc pieniędzy na koncie nie może zejść poniżej 0.0, np. jeżeli z konta na którym jest 300zł próbujemy wypłacić 500zł to metoda zwróci nam tylko 300zł.
Pamiętaj o sprawdzeniu czy podana wartość jest:
 * Wartością numeryczną,
 * Wieksza od 0.0
6. Posiadać metodę `print_info` nie przyjmującą żadnych parametrów. Metoda ta ma wyświetlić informację o numerze konta i jego stanie.

### Zadanie 4.
Stwórz klasę `Employee`, która ma spełniać następujące wymogi:

1. Mieć publiczne atrybuty:
 * `id` - atrybut ten powinien trzymać numer identyfikacyjny pracownika,
 * `first_name` - atrybut określający imię pracownika,
 * `last_name` - atrybut określający nazwisko pracownika,
 * `salary` - atrybut określający ile pracownik zarabia za godzinę.
2. Posiadać konstruktor przyjmujący id, imię, nazwisko i płace za godzinę.
3. Posiadać metodę `raise_salary(percent)` której rolą będzie zwiększenie wartości atrybutu `salary` o podany procent. Pamiętaj o sprawdzeniu czy podana wartość jest:
 * Wartością numeryczną,
 * Wieksza (lub równa) od 0.0
