# Zmienne instancyjne &ndash; zadania.


### Zadanie 1.
W konstruktorze klasy `Cat` przyjmuj parametr `name` i zapisz go w zmiennej instancyjnej `@name`. Dopisz atrybut (akcesor) umożliwiający tylko odczyt tej zmiennej.

Przetestuj działanie tworząc parę obiektów klasy `Cat` i wypisując na ekran ich atrybuty `name`.

### Zadanie 2.
W konstruktorze klasy `Dog` przyjmuj parametr `age` i zapisz go w zmiennej instancyjnej `@age`.

Dopisz atrybut (akcesor) umożliwiający tylko odczyt tej zmiennej.

### Zadanie 3.
Napisz klasę `Human`. W konstrukturze przyjmuj parametry `name` i `surname`.

Dopisz atrybuty (akcesory) umożliwiający tylko odczyt imienia, ale odczyt i zapis nazwiska.


### Zadanie 4.
Stwórz klasę Person posiadającą następujące dane takie jak:
- name
- surname
- age
- gender

Stwórz odpowiedni `initialize`, który przyjmuje wszystkie argumenty.
Dodaj metody dostępowe do zmiennych obiektu. Stwórz przykładowe obiekty.