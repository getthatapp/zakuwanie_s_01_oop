# Metody &ndash; zadania.

### Zadanie 1.
Do klasy `Human` dopisz metodę `full_name`, która wypisze imię i nazwisko połączone znakiem spacji.

### Zadanie 2.
Do klasy `Dog` napisz metodę `birthday`, które zinkrementuje zmienną `@age` o 1, a następnie zwróci komunikat "Wesołych urodzin psiaku!"

### Zadanie 3.
Stwórz klasę Car posiadającą następujące atrybuty:
- brand
- model
- price

Stwórz metody dostępowe do atrybutów.
Stwórz metodę `show` - wyświetlającą markę oraz cenę w jednej linii.