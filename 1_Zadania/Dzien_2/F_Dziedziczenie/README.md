# Dziedziczenie &ndash; zadania

### Zadanie 1 &ndash; rozwiązywane z wykładowcą.
Stwórz klasę ```AdvancedCalculator```, która dziedziczy po klasie ```Calculator```.
Klasa powinna implementować następujące metody:

1. ```pow(num1, num2)``` &ndash; metoda ma zwracać ```num1``` do potęgi ```num2```. Dodatkowo w tablicy operacji ma zapamiętać napis: "```num1```^```num2``` equals ```result```".
2. ```root(num1, num2)``` &ndash; metoda ma wyliczyć pierwiastek ```num2``` stopnia z ```num1```. Dodatkowo w tablicy operacji ma zapamiętać napis: "root ```num2``` of ```num1``` equals ```result```".  

---

### Zadanie 2.
Stwórz klasę `Circle`, która ma spełniać następujące wymogi:

1. Dziedziczyć po klasie definiującej kształt.
2. Mieć dodatkowy atrybut `promień`.
3. Mieć konstruktor przyjmujący zmienne określające wartości `x`, `y`, `kolor` i `promień`.  Konstruktor powinien wypisywać informacje o właśnie stworzonym okręgu. Pamiętaj o sprawdzeniu:
 * czy podane zmienne są liczbami (jeżeli nie są, to nastaw oba na **0**),
 * czy podany kolor jest napisem.
4. Nadpisywać funkcje kształtu (nadpisz tylko te, które tego wymagają).
5. Mieć funkcję zwracającą pole powierzchni.
6. Mieć funkcję zwracającą obwód.

### Zadanie 3.
Stwórz klasę `HourlyEmployee`, która ma reprezentować pracownika któremu pracodawca płaci za godziny. Klasa ma spełniać następujące wymogi:
 1. Dziedziczyć po klasie `Employee`.
 2. Mieć dodatkową metodę `compute_payment(hours)` która będzie zwracała kwotę do wypłacenia pracownikowi za liczbę wypracowanych godzin. 

### Zadanie 4.
Stwórz klasę `SalariedEmployee`, która ma reprezentować pracownika z którym pracodawca ma umowę miesięczną. Klasa ma spełniać następujące wymogi:
 1. Dziedziczyć po klasie `Employee`.
 2. Mieć dodatkową metodę `computePayment()` która będzie zwracała kwotę do wypłacenia pracownikowi za miesiąc (dla uproszczenia możemy założyć że w każdym miesiącu jest 190 godzin pracujących). 
