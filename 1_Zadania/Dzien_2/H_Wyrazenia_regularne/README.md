# Wyrażenia regularne

### Zadanie 1 &ndash; zadanie rozwiązywane z wykładowcą
Napisz klasę PostalCode. Klasa ma mieć metodę statyczną `is_postal_code_valid?(code)`, która sprawdzi czy podany kod pocztowy jest zgodny z formatem XX-XXX i zwróci true lub false.

---

### Zadanie 2.
Zmień klasę PostalCode, tak by przy jej tworzeniu konstruktor przyjmował jako parametr kod pocztowy i zapisywał go w zmiennej instancyjnej.
Dopisz metodę `is_valid?`, która sprawdzi czy podany kod jest zgodny z formatem XX-XXX i zwróci true. Jeżeli kod pocztowy jest w formacie XXXXX, klasa powinna go wtedy zmienić na format XX-XXX. W przeciwnym wypadku zwróci false.


### Zadanie 3.
Napisz prosty program konsolowy, który przyjmuje od użytkownika numer telefonu i sprawdza (wypisując na ekran wynik) czy jest on w poprawnym polskim formacie (jako poprawny polski format przyjmijmy +48 XXX XXX XXX).