# Zaawansowana obiektowość

### Zadanie 1 &ndash; zadanie rozwiązywane z wykładowcą
Do klasy ```AdvancedCalculator``` dopisz:

 1. Stałą  ```PI```, który będzie miał przypisaną wartość **3.14159265**.
 2. Statyczną funkcję ```compute_circle_radius(r)``` która będzie zwracała pole koła. Ta metoda nie będzie dopisywać obliczeń do tablicy (napisz w komentarzu, dlaczego nie może tego robić).

---

### Zadanie 2.
Zmień klasę `BankAccount` w taki sposób żeby konstruktor sam nadawał numer konta bankowego. Dla uproszczenia będziemy nadawać kolejne liczby całkowite zaczynając od jedynki. Zeby to zrobić:
 1. Dodaj do klasy zmienną klasową `next_acc_number`, ustawioną standardowo na 1.
 2. Zmodyfikuj konstruktor w taki sposób żeby nie przyjmował numeru konta, tylko przypisywał wartość `next_acc_number` do swojego atrybutu `number`, a nastepnie zwiększał `next_acc_number` o jeden.

Przetestuj jak generowane są numery twoich kont.

### Zadanie 3.
Napisz klasę `MyClass` i moduł `MyModule`.

* W module napisz metodę `to_s`, zwracającą "Jestem obiektem!"
* Zaincluduj moduł w klasie `MyClass` i sprawdź działanie wypisując string z instancją tej klasy używając interpolacji.

### Zadanie 4.
Napisz klasę `MySecondClass` i moduł `MySecondModule`.

* W module napisz metodę `to_s`, zwracającą "Jestem klasą!"
* Zaextenduj moduł w klasie `MySecondClass` i sprawdź działanie wypisując string z tą klasą (nie instancją klasy!) używając interpolacji.

### Zadanie 5.
Napisz definicję klasy `Cat`.
* Klasa zawiera konstruktor nie przyjmujący żadnych argumentów i ustawiający zmienną instancyjną `@counter` na wartość 0, a także metodą `meow` zwracającą tekst "meow number " i wartość zmiennej counter (czyli np. meow number 3), a następnie inkrementującą tę zmienną.
* Klasa ma mieć ustawione akcesory dla zmiennej counter.
* Klasa w żadnym momencie nie wypisuje nic na ekran.

### Zadanie 6.
Napisz moduł udostępniający metodę `hau`, która zwróci `hau hau`. Dołącz tę metodę do klasy `Cat` i przetestuj ją na instancji kota.

### Zadanie 7.
Napisz klasę `Animal`, a następnie dziedziczące po niej - `Dog`, `Raccoon` i `Cat`. Wszystkie te klasy mają mieć zmienną klasową `@@animal_counter`, inkrementowaną przy tworzeniu każdego nowego obiektu.

Następnie napisz moduł `AnimalCounter`, który doda do każdej z tych klas metodę `animal_count` (klasową i instancyjną) zwracającą aktualną liczbę zwierząt.

Sprawdź przy okazji jak dziedziczenie ma się do zmiennych klasowych (są dziedziczone, tworzą się kopie czy każda ma swoją osobną).