# Rubocop

### Zadanie 1 - robione z wykładowcą
Uruchom Rubocopa na przykładzie kodu w plik bad_code.rb.
Popraw wszystkie znalezione błędy.

---

### Zadanie 2
Przejdź do folderu 1_Zadania/
Uruchom w nim Rubocopa i popraw wszystkie znalezione błędy.

### Zadanie 3
Uruchom Rubocopa na swojej pracy domowej z poprzedniego dnia. Popraw znalezione błędy.