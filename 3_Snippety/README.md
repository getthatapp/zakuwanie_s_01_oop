<img alt="Logo" src="http://coderslab.pl/wp-content/themes/coderslab/svg/logo-coderslab.svg" width="400">


# Programowanie obiektowe w Ruby
> Kilka ważnych informacji


Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki:


#### Jak zdefiniować klasę?

```ruby
class Cat
end
```

#### Jak utworzyć nowy obiekt klasy?

```ruby
c = Cat.new
```

#### Jak napisać konstruktur dla klasy?
```ruby
class Cat
  def initialize
    #zawartosc
  end
end
```

#### Jak zdefiniować akcesory?
```ruby
class Cat
  attr_reader :name # pozwoli na odczyt
  attr_writer :write_only # pozwoli na zapis
  attr_accessor :age # pozwoli na odczyt i zapis
end
```

#### Jak zainicjalizować zmienną wartością?
```ruby
class Cat
  attr_reader :name # pozwoli na odczyt

  def initialize(name)
    @name = name
  end
end
```

#### Jak zdefiniować metodę wewnątrz klasy?
```ruby
class Cat
  def moja_metoda
    # tresc metody
  end
end
```


#### Jak zdefiniować, odczytać i zapisać zmienną klasową?
```ruby
class Cat

  @@class_variable = 0

  def get_class_var
    @@class_variable
  end

  def set_class_var(v)
    @@class_variable = v
  end
end
```

#### Jak odwołać się do samego siebie wewnątrz klasy?
```ruby
class Cat
  def get_self
    self
  end
end
```

#### Jak nadpisać funkcję rzutowania do stringa?
```ruby
class Cat
  def to_s
    "Jestem kotem"
  end
end

c = Cat.new
puts c # => Jestem kotem
```

#### Jak zdefiniować własne akcesory?
```ruby
class Account
  def initialize
    @balance = 0
  end

  def balance
    @balance/100.0
  end

  def balance=(balance)
    @balance = (balance * 100).to_i
  end

  def internal_balance
    @balance
  end
end

account = Account.new
account.balance = 12.0
puts account.balance # => 12.0
puts account.internal_balance # => 1200
```

#### Jak zapisać dziedziczenie?
```ruby
class Book
end

class Ebook < Book
end
```


#### Jak wywołać konstruktor rodzica przy dziedziczeniu?
```ruby
class EBook < Book

  def initialize
    super
  end
end
```

#### Jak nadpisać metodę w klasie potomnej?
```ruby
class Book
  def metoda
  end
end

class Ebook < Book
  def metoda
    super # wywoła metodę z klasy rodzica
  end
end
```

#### Jak zdefiniować i wywołać metodę klasową?
```ruby
class Book
  def self.class_method
  end
end

Book.class_method
```

#### Jak napisać moduł?
```ruby
module MyModule
end
```

#### Jak użyć modułu jako zbiór metod instancyjnych?
```ruby
module MyModule
  def metoda
  end
end

class MyClass
  include MyModule
end

m = MyClass.new
m.metoda
```

#### Jak użyć modułu jako zbiór metod klasowych?
```ruby
module MyModule
  def metoda
  end
end

class MyClass
  extend MyModule
end

MyClass.metoda
```

#### Jak użyć modułu jako przestrzeni nazw?
```ruby
module Volkswagen
  class Engine
    def run
      "silnik vw rusza"
    end
  end
end

class Engine
  def run
    "generyczny silnik rusza"
  end
end

puts Volkswagen::Engine.new.run # silnik vw rusza
puts Engine.new.run # generyczny silnik rusza
```

#### Jak zdefiniować stałą w przestrzeni nazw?
```ruby
class Match
  POINTS_FOR_WIN = 3
end


module Constant
  E = 2.71828
end

puts Match::POINTS_FOR_WIN # => 3
puts Constant::E # => 2.71828
```

#### Jak sprawdzić dopasowanie wyrażenia regularnego?
```ruby
regexp = /(\w+)=\1/
string = "Ala=Ala Ala=Ala"

string =~ regexp # => 0, bo jest to indeks pierwszego dopasowania
string.scan(regexp) # => [["Ala"], ["Ala"]], bo zwraca wszystkie dopasowania
```

#### Jak zainstalować Rubocopa?
```
gem install rubocop
```

#### Jak sprawdzić plik Rubocopem?
```
rubocop ruby_plik.rb
```

#### Jak sprawdzić folder Rubocopem?
```
rubocop folder/
```

#### Jak sprawdzić obecny folder Rubocopem?
```
rubocop
```

#### Gdzie zapisać konfigurację Rubocopa?
```
.rubocop.yml
```

#### Jak wygląda zalecana przez nas konfiguracja Rubocopa?
```
Documentation:
  Enabled: false
```
